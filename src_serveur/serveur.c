/******************************************************************/
/* Fichier        : serveur.c                                     */
/* Emplacement    : Src_serveur                                   */
/* Auteur         : BOUCKAERT Benoit - VICIOT Léa                 */
/* Description    : Exécutable d'un serveur chat                  */
/*                                                                */
/******************************************************************/

/*** Fichier d'inclusions ***/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libreseau.h"
#include "libflux.h"
#include "libliste_clients.h"
#include "protocole_perso.h"
#include "libflux.h"

/*** Constantes ***/
#define MAX_UDP_MESSAGE 5
#define DELAY_DIFFUSION_UDP 1000000
#define LEN_PSEUDO 10

#define LOCK_LISTE_CLIENTS 0
#define LOCK_ENVOI_MESSAGE 1

#define ADRESSE_DIFFUSION "255.255.255.255" 

/*** Variables globales ***/
liste_clients_t * liste_clients;

/*** Fonctions ***/

/* Emission de la balise UDP */
void thread_emetteur_UDP(void* arg)
{
    int s=initialisation_serveur_UDP(P_UDP_BALISE);
    void* adresse = creation_addresse_UDP(ADRESSE_DIFFUSION, P_UDP_BALISE);

    unsigned char nom_serveur[LM_NOM_SERVEUR] = "Serveur Lea Benoit";
    
    do
    {
        envoyer_UDP(s, adresse, nom_serveur, LM_NOM_SERVEUR);
        usleep(DELAY_DIFFUSION_UDP);

    } while (s>0);
}

/*  Envoi d'un message */
void envoi_message_tous(char* pseudo, char* message, liste_clients_t * liste, int envoyeur)
{
    liste_clients_t* _liste = liste;

    char _message[MAX_TCP_MESSAGE];
            
    memset(_message, 0, MAX_TCP_MESSAGE);
    strcat(_message, pseudo);
    strcat(_message, " : ");
    strcat(_message, message);
    printf("%s", _message);

    semaphore_lock(LOCK_LISTE_CLIENTS);
    semaphore_lock(LOCK_ENVOI_MESSAGE);

    while(_liste != NULL)
    {
        if(_liste->socketfd != envoyeur)
        {
            write(_liste->socketfd, (void*)_message, MAX_TCP_MESSAGE);
        }
        _liste = _liste->suivant;
    }

    semaphore_unlock(LOCK_ENVOI_MESSAGE);
    semaphore_unlock(LOCK_LISTE_CLIENTS);
}

/* Fonction thread client */
void thread_client(void* arg)
{
    int     client = *(int*)arg;
    char    message[MAX_TCP_MESSAGE];
    char    pseudo[LEN_PSEUDO];
    int     len;

    semaphore_lock(LOCK_LISTE_CLIENTS);
    liste_clients = ajouter_client(liste_clients, client);
    semaphore_unlock(LOCK_LISTE_CLIENTS);

    len = read(client, pseudo, LEN_PSEUDO);
    envoi_message_tous(pseudo, "Connecté\n", liste_clients, client);

    do
    {
        memset(message, 0, MAX_TCP_MESSAGE);
        len = read(client, (void*)message, MAX_TCP_MESSAGE);
        
        if(len < 0)
        {
            perror("thread_client.read");
            exit(EXIT_FAILURE);
        }

        if(len != 0)
            envoi_message_tous(pseudo, message, liste_clients, client);


    } while(len != 0);

    semaphore_lock(LOCK_LISTE_CLIENTS);
    liste_clients = supprimmer_client(liste_clients, client);
    semaphore_unlock(LOCK_LISTE_CLIENTS);

    envoi_message_tous(pseudo, "déconnecté.\n", liste_clients, 0);
}

/* Fonction de traitement d'un client */
void traitement_client(int id_client)
{
   creer_thread(thread_client, (void*)&id_client, sizeof(id_client));
}

/* Fonction du jeu */
void jeu(void* arg)
{
    /* A faire : Benoît */
}


/*** Fonction principale ***/
int main()
{
    /** Création des sémaphores **/
    int statut = init_semaphore(LOCK_LISTE_CLIENTS);
    if(statut<0)
    {
        perror("main.init_semaphore (LOCK_LISTE_CLIENTS)");
        exit(EXIT_FAILURE);
    }

    statut = init_semaphore(LOCK_ENVOI_MESSAGE);
    if(statut<0)
    {
        perror("main.init_semaphore (LOCK_ENVOI_MESSAGE)");
        exit(EXIT_FAILURE);
    }

    /** Initialisation du serveur **/
    int id_socket = init_serveur_TCP(P_TCP_CHAT, 1); 

    if (id_socket < 0)
    {
        perror("main.init_serveur");
        exit(EXIT_FAILURE);
    }

    /** Création des threads des fonctions **/
    creer_thread(thread_emetteur_UDP, NULL, 0);

    /** Appel de la fonction boucle serveur **/
    boucle_serveur_TCP(id_socket, traitement_client);
    
    return 0;
}
    

   
