#ifndef __ENTITES_H__
#define __ENTITES_H__

/**** Structures ****/

typedef struct coord
{
    int x;
    int y;
} Coordonnees;

typedef struct entite
{
    unsigned char   id_ent;
    Coordonnees *   pos;
    unsigned int    rayon;
    int    orientation;
} Entite;

typedef struct mur
{
    Coordonnees *   points[2];    
} Mur;

typedef struct l_ent
{
    Entite*         entite;
    struct l_ent*   suivant;
} Liste_entites;

typedef struct l_mur
{
    Mur*            mur;
    struct l_mur*   suivant;
} Liste_murs;

typedef struct
{
    int type;
    union
    {
        Entite  e;
        Mur     m;
    };
} Objet;

typedef struct l_objet
{
    Objet           o;
    struct l_objet* suivant;
} Liste_objets;

/**** Constantes ****/

#define TYPE_MUR    0
#define TYPE_ENTITE 1

/**** Prototypes de fonctions ****/

/*** Gestion des structures ***/

/** Structure Coordonnees **/

Coordonnees* new_coordonnees(int x, int y);
void free_coordonnees(Coordonnees* coordonnees);

/** Structure Entites **/

Entite* new_entite(int id_ent, int x, int y, int rayon, int orientation);
void free_entite(Entite* entite);

/** Strucutre Mur **/

Mur* new_mur(int xA, int yA, int xB, int yB);
void free_mur(Mur* mur);

/*** Gestion des listes ***/

/** Liste des entités **/
Liste_entites * ajouter_entite(Liste_entites* liste, Entite* entite);
Liste_entites * enlever_entite(Liste_entites* liste, Entite* entite);
Liste_entites * duplique_liste_entites(Liste_entites* liste);
void vider_liste_entites(Liste_entites * liste);

/** Liste des murs **/
Liste_murs * ajouter_mur(Liste_murs* liste, Mur* mur);
Liste_murs * duplique_liste_murs(Liste_murs* liste);
void tri_liste_murs(Liste_murs* liste);
Liste_murs * enlever_mur(Liste_murs* liste, Mur* mur);
void vider_liste_murs(Liste_murs * liste);

/** Liste d'objets **/
Liste_objets * ajouter_murs(Liste_objets* liste, Liste_murs* murs);
Liste_objets * ajouter_entites(Liste_objets* liste, Liste_entites* entites);
void trie_liste_objets(Liste_objets* liste);
void vider_liste_objets(Liste_objets* liste);

#endif