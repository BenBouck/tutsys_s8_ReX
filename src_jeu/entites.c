#include <stdlib.h>
#include <math.h>
#include "entites.h"

/*** Fonctions ***/


/** Gestion de la structure Coordonnees **/

/* Allocation d'un nouveau point */
Coordonnees* new_coordonnees(int x, int y)
{
    /* Déclaration de la variable de retour */
    Coordonnees* coordonnees = NULL;

    /* Allocation de la mémoire */
    coordonnees = malloc(sizeof(Coordonnees));
    
    /* Assignation des valeurs sur réussite du malloc */
    if(coordonnees != NULL)
    {
        coordonnees->x = x;
        coordonnees->y = y;
    }

    /* Retour de la structure générée */
    return coordonnees;
}

/* Libération de l'espace mémoire d'un point */
void free_coordonnees(Coordonnees* coordonnees)
{
    if(coordonnees != NULL)
    {
        /* Mise à 0 de la mémoire */
        coordonnees->x = 0;
        coordonnees->y = 0;

        /* Libération */
        free(coordonnees);
    }
}


/** Gestion de la structure Entites **/

/* Allocation d'une nouvelle entite */
Entite* new_entite(int id_ent, int x, int y, int rayon, int orientation)
{
    /* Déclaration de la variable de retour */
    Entite* entite = NULL;

    /* Allocation de la mémoire */
    entite = malloc(sizeof(Entite));
    
    /* Assignation des valeurs données sur réussite du malloc */
    if(entite != NULL)
    {
        entite->id_ent = id_ent;
        entite->rayon = rayon;
        entite->orientation = orientation;
        entite->pos = new_coordonnees(x, y);
    }

    /* Retour de la structure générée */
    return entite;
}

/* Libération de l'espace mémoire d'une entité */
void free_entite(Entite* entite)
{
    if(entite != NULL)
    {
        /* Libération des structures contenues */
        free_coordonnees(entite->pos);

        /* Mise à 0 de la mémoire */
        entite->pos = NULL;
        entite->id_ent = 0;
        entite->orientation = 0;
        entite->rayon = 0;
        
        /* Libération */
        free(entite);
    }
}


/** Gestion de la strucutre Mur **/

/* Allocation d'un nouveau mur */
Mur* new_mur(int xA, int yA, int xB, int yB)
{
    /* Déclaration de la variable de retour */
    Mur* mur = NULL;

    /* Allocation de la mémoire */
    mur = malloc(sizeof(Mur));

    /* Assignation des valeurs données sur réussite du malloc */
    if(mur != NULL)
    {
        mur->points[0] = new_coordonnees(xA, yA);
        mur->points[1] = new_coordonnees(xB, yB);
    }

    /* Retour de la structure générée */
    return mur;
}

/* Libération de l'espace mémoire d'un mur */
void free_mur(Mur* mur)
{
    if(mur != NULL)
    {
        /* Libération des structures contenues */
        free_coordonnees(mur->points[0]);
        free_coordonnees(mur->points[1]);

        /* Mise à 0 de la mémoire */
        mur->points[0] = NULL;
        mur->points[1] = NULL;

        /* Libération */
        free(mur);
    }
    
}


/** Gestion d'une liste d'entités **/

/* Ajout d'un élément dans la liste */
Liste_entites * ajouter_entite(Liste_entites* liste, Entite* entite)
{
    if(liste != NULL)
    {
        /* Si élément déjà existant, ajout à l'élément suivant par récursivité */
        liste->suivant = ajouter_entite(liste->suivant, entite);
    }
    else
    {
        /* Sinon allocation de la mémoire */
        liste = malloc(sizeof(Liste_entites));
        
        /* Assignation des éléments sur réussite du malloc */
        if(liste != NULL)
        {
            liste->entite = entite;
            liste->suivant = NULL;   
        }
    }

    /* Retour de l'index de liste */
    return liste;
}

/* Retrait d'un élément dans la liste */
Liste_entites * enlever_entite(Liste_entites* liste, Entite* entite)
{
    if(liste != NULL)
    {
        /* Si élément non vide */
        if(liste->entite->id_ent == entite->id_ent)
        {
            /* Variable tampon */
            Liste_entites* tmp = liste->suivant;
            
            /* Suppression de l'élément présent */
            liste->suivant = NULL;
            free_entite(liste->entite);
            liste->entite = NULL;
            free(liste);

            /* Assignation pour retour */
            liste = tmp;
        }
        else
        {
            /* Si mauvais élément, passage à l'élément suivant */
            liste->suivant = enlever_entite(liste->suivant, entite);
        }
    }

    /* Retour de la liste */
    return liste;
}

/* Duplication de la liste */
Liste_entites * duplique_liste_entites(Liste_entites* liste)
{
    Liste_entites* liste_copie = NULL;
    if(liste != NULL)
    {
        Entite* entite = new_entite(liste->entite->id_ent, liste->entite->pos->x, liste->entite->pos->y, liste->entite->rayon, liste->entite->orientation);    
        liste_copie = ajouter_entite(NULL, entite);
        liste_copie->suivant = duplique_liste_entites(liste->suivant);
    }
    return liste_copie;
}

/* Libération de l'espace mémoire utilisé par la liste */
void vider_liste_entites(Liste_entites * liste)
{
    if(liste != NULL)
    {
        /* Libération de l'élément suivant */
        vider_liste_entites(liste->suivant);
        
        free_entite(liste->entite);
        
        /* Mise à zéro de la mémoire */
        liste->suivant = NULL;
        liste->entite = NULL;

        /* Libération */
        free(liste);
    }
}

/** Gestion d'une liste de murs **/

/* Ajout d'un élément dans la liste */
Liste_murs * ajouter_mur(Liste_murs* liste, Mur* mur)
{
    /* Fonctionnement, cf. liste entités (TODO) */
    if(liste == NULL)
    {
        liste = malloc(sizeof(Liste_murs));
        if(liste != NULL)
        {
            liste->mur = mur;
            liste->suivant = NULL;
        }
    }
    else
    {
        liste->suivant = ajouter_mur(liste->suivant, mur);
    }
    return liste;
}

/* Duplication de la liste */
Liste_murs * duplique_liste_murs(Liste_murs* liste)
{
    Liste_murs* liste_copie = NULL;
    if(liste != NULL)
    {
        Mur* mur = new_mur(liste->mur->points[0]->x, liste->mur->points[0]->y, liste->mur->points[1]->x, liste->mur->points[1]->y);
        liste_copie = ajouter_mur(NULL, mur);
        liste_copie->suivant = duplique_liste_murs(liste->suivant);
    }
    return liste_copie;
}

int _cmp_murs(Mur mur01, Mur mur02)
{
    int cx1, cy1, cx2, cy2, d1, d2;
    cx1 = (mur01.points[0]->x + mur01.points[1]->x)/2;
    cy1 = (mur01.points[0]->y + mur01.points[1]->y)/2;
    cx2 = (mur02.points[0]->x + mur02.points[1]->x)/2;
    cy2 = (mur02.points[0]->y + mur02.points[1]->y)/2;
    d1 = abs(cx1) + abs(cy1);
    d2 = abs(cx2) + abs(cy2);
    return (d2>d1)?1:0;
}

/* Tri de la liste de murs (tri à bulles) */
void tri_liste_murs(Liste_murs* liste)
{
    char echange;
    Liste_murs* tmp_mur1;
    Liste_murs* tmp_mur2 = NULL;
    if(liste != NULL)
    {
        do
        {
            echange = 0;
            tmp_mur1 = liste;

            while (tmp_mur1->suivant != tmp_mur2)
            {
                if(_cmp_murs(*tmp_mur1->mur, *tmp_mur1->suivant->mur))
                {
                    Mur* tmp = tmp_mur1->mur;
                    tmp_mur1->mur = tmp_mur1->suivant->mur;
                    tmp_mur1->suivant->mur = tmp;
                    echange = 1;
                }
                tmp_mur1 = tmp_mur1->suivant;
            }
            tmp_mur2 = tmp_mur1;
            
        } while (echange);
    }
}

/* Retrait d'un élément de la liste */
Liste_murs * enlever_mur(Liste_murs* liste, Mur* mur)
{
    if(liste != NULL)
    {
        if(liste->mur == mur)
        {
            Liste_murs* tmp = liste->suivant;

            free_mur(liste->mur);
            liste->suivant = NULL;
            liste->mur = NULL;

            free(liste);

            liste = tmp;
        }
        else
        {
            liste->suivant = enlever_mur(liste->suivant, mur);
        }
    }
    return liste;
}

/* Libération de l'espace mémoire utilisé par la liste */
void vider_liste_murs(Liste_murs * liste)
{
    if(liste != NULL)
    {
        vider_liste_murs(liste->suivant);
        free_mur(liste->mur);

        liste->mur = NULL;
        liste->suivant = NULL;
        
        free(liste);
    }
}

/** Gestion d'une liste d'objets **/

/* Ajoute une liste de murs à la liste d'objets */
Liste_objets * ajouter_murs(Liste_objets* liste, Liste_murs* murs)
{
    if(murs != NULL)
    {
        Liste_objets* nliste = malloc(sizeof(Liste_objets));
        nliste->o.type = TYPE_MUR;
        nliste->o.m = *(murs->mur);
        nliste->suivant = liste;
        return ajouter_murs(nliste, murs->suivant);;
    }
    else
    { 
        return liste;
    }
}

/* Ajoute une liste d'entites à la liste d'objets */
Liste_objets * ajouter_entites(Liste_objets* liste, Liste_entites* entites)
{
    if(entites != NULL)
    {
        Liste_objets* nliste = malloc(sizeof(Liste_objets));
        nliste->o.type = TYPE_ENTITE;
        nliste->o.e = *(entites->entite);
        nliste->suivant = liste;
        return ajouter_entites(nliste, entites->suivant);
    }
    else
    {
        return liste;
    }
}

/* Fonction de calcul de la distance d'un objet */
int _dist_obj(Objet o)
{
    switch(o.type) 
    {
        int dx,dy;
        case TYPE_MUR :
            dx = (o.m.points[0]->x + o.m.points[1]->x)/2;
            dy = (o.m.points[0]->y + o.m.points[1]->y)/2;
            return abs(dx)*abs(dx) + abs(dy)*abs(dy);
            break;
        case TYPE_ENTITE :
            return abs(o.e.pos->x)+abs(o.e.pos->x) + abs(o.e.pos->y)*abs(o.e.pos->y) - o.e.rayon;
            break;
        default:
            return -1;
            break;
    }

    /* On fait taire le compilateur */
    return -1;
}

/* Fonction locale de tri d'objets */
int _cmp_objs(Objet o1, Objet o2)
{
    return (_dist_obj(o2) > _dist_obj(o1))?1:0;
}

/* Trie la liste d'objets du plus lointain au plus proche */
void trie_liste_objets(Liste_objets* liste)
{
    char echange;
    Liste_objets* tmp_obj1;
    Liste_objets* tmp_obj2 = NULL;
    if(liste != NULL)
    {
        do
        {
            echange = 0;
            tmp_obj1 = liste;

            while (tmp_obj1->suivant != tmp_obj2)
            {
                if(_cmp_objs(tmp_obj1->o, tmp_obj1->suivant->o))
                {
                    Objet tmp_sw = tmp_obj1->o;
                    tmp_obj1->o = tmp_obj1->suivant->o;
                    tmp_obj1->suivant->o = tmp_sw;
                    echange = 1;
                }
                tmp_obj1 = tmp_obj1->suivant;
            }
            tmp_obj2 = tmp_obj1;
            
        } while (echange);
    }
}


void vider_liste_objets(Liste_objets* liste)
{
    if(liste != NULL)
    {
        vider_liste_objets(liste->suivant);
        free(liste);
    }
}