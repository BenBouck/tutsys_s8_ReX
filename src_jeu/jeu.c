/**** Programme de creation de labyrinthe ****/

/** Fichiers d'inclusion **/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include "libgraph.h"
#include "entites.h"
#include "hitboxes.h"
#include "mouvement.h"
#include "projecteur.h"
#include "objets2D.h"
#include "fenetre.h"
#include "afficheur.h"
#include "moteur_jeu.h"

/** Constantes **/

#define ATTENTE		10000

/** Variables globales **/

char*       laby[2*LABY_Y+1]={
  " - - - - - - - - ",
  "|. . . . . . . .|",
  "                 ",
  "|. .|. . . . . .|",
  "       - -       ",
  "|. .|.|. .|. . .|",
  "                 ",
  "|. .|.|. .|. . .|",
  "                 ",
  "|. .|.|. .|. . .|",
  "       - -       ",
  "|. .|.|. . . . .|",
  "                 ",
  "|. .|.|. . . . .|",
  "                 ",
  "|. . . . . . . .|",
  " - - - - - - - - "
  };

/** Fonctions **/

/* Calcul des murs en fonction du dessin */

Liste_murs* dessin_vers_murs(char *laby[], Liste_murs* murs)
{
  int i;
  for(i=0;i<9;i++)
  {
    int j;
    for(j=0;j<9;j++)
    {
      if(laby[2*i][2*j+1]=='-')
      {
        Mur* mur_act = new_mur(j*MUR_HAUTEUR, i*MUR_HAUTEUR, (j+1)*MUR_HAUTEUR, i*MUR_HAUTEUR);
        murs = ajouter_mur(murs, mur_act);
      }
      if(i<8 && laby[2*i+1][2*j]=='|')
      {
        Mur* mur_act = new_mur(j*MUR_HAUTEUR, i*MUR_HAUTEUR, j*MUR_HAUTEUR, (i+1)*MUR_HAUTEUR);
        murs = ajouter_mur(murs, mur_act);
      }
    }
  }
  return murs;
}


/* Fonction principale */

int main(void)
{
  /* Déclaration de variables */
  unsigned char   resultat  = 0;
  int statut;

  /* Initialisation d'une fenêtre */
  resultat = creerFenetre(LARGEUR_FENETRE, HAUTEUR_FENETRE, TITRE);
  if(!resultat)
  { 
    fprintf(stderr,"Problème graphique !\n"); 
    exit(-1); 
  }
  
  /* Création des murs à partir du dessin */
  Liste_murs * murs = NULL;
  murs = dessin_vers_murs(laby, murs);

  statut = jeu(murs);

  switch (statut)
  {
    case JEU_GAGNE:
      printf("Vous avez gagné !\n");
      break;
    case JEU_PERDU:
      printf("Vous avez perdu ...\n");
      break;
    case JEU_QUITTE:
      printf("Fin de la partie.\n");
      break;   
    default:
      break;
  }

  /* Fermeture de la fenêtre */
  fermerFenetre();

  return 0;
}
