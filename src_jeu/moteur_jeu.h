#ifndef __MOTEUR_JEU_H__
#define __MOTEUR_JEU_H__

#include "entites.h"

#define LABY_X	8
#define LABY_Y	8
#define ATTENTE 10000
#define CADENCE 5      //Tirs par seconde

#define JEU_QUITTE  0
#define JEU_GAGNE   1
#define JEU_PERDU   2

/* Comportement du mob */

#define P_AVANCER   50
#define P_GAUCHE    15
#define P_DROITE    25
#define P_RECULER   1
#define P_TIRER     9

#define ID_PLAYER   0
#define ID_MOB      1

int jeu(Liste_murs* murs);

#endif