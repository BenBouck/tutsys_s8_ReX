#ifndef __PROTOCOLE_PERSO_H__
#define __PROTOCOLE_PERSO_H__

/*** Constantes ***/

/** Longueurs maximales **/
#define LM_NOM_SERVEUR  20
#define LM_PSEUDO       20
#define LM_MESSAGE      255

/** Définition des ports **/
#define P_TCP_CHAT      "4550"
#define P_UDP_BALISE    "4551"
#define P_UDP_TOUCHES   "4552"
#define P_UDP_GRAPH     "4553"

/** Commandes **/
#define CMD_ID          "\\"        //Les commandes commencent par un anti-slash
#define CMD_PSD         "PS"        //Envoyé au serveur à la connexion, suivi du pseude
#define CMD_IDPL        "ID"        //Envoyé au client à sa connexion, suivi d'un ID unique
#define CMD_START       "GO"        //Envoyé au serveur pour démarrer une partie
#define CMD_STOP        "ST"        //Envoyé au serveur pour arrêter une partie
#define CMD_KO          "KO"        //Envoyé aux clients en cas de crash
#define CMD_SHUTDOWN    "XX"        //Envoyé au serveur pour l'éteindre, et aux clients pour prévenir d'une extinction

/** Rangs des bits pour les touches **/
#define KEY_UP          0
#define KEY_DOWN        1
#define KEY_LEFT        2
#define KEY_RIGHT       3
#define KEY_SPACEBAR    4

/** Formes **/
#define MAX_FORMES  200
#define ST_CERCLE   0
#define ST_RECT     1

/*** Structures ***/

/** Graphique **/

/* Point */
typedef struct 
{
    unsigned int x;
    unsigned int y;
}Point2D_t;


/* Objet d'affichage */
/*typedef struct
{
    unsigned char type;
    union forme
    {
        Point2D_t p[4];
        struct c
        {
            Point2D_t centre;
            unsigned int rayon;
        };
        
    };
}Objet2D_t;*/


/** Communication **/

/* Balise UDP */
typedef struct 
{
    unsigned char *nom_serveur;
} Balise_UDP_t;

/* Touches */
typedef unsigned char Touches_t;

/* Paquet formes */
/*typedef struct
{
    unsigned    int nb;
    Objet2D_t   formes[MAX_FORMES];
} Envoi_formes_t;*/

#endif