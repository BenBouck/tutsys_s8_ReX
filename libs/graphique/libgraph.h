/**** Bibliotheque graphique (definitions) ****/

/** Constantes **/

#define COULEUR_BLANC	0
#define COULEUR_NOIR	1
#define COULEUR_ROUGE	2
#define COULEUR_VERT	3
#define COULEUR_BLEU	4
#define COULEUR_ROSE	5
#define COULEUR_JAUNE   6

#define TOUCHE_DROITE	0
#define TOUCHE_GAUCHE	1
#define TOUCHE_HAUT	    2
#define TOUCHE_BAS	    3
#define TOUCHE_ESPACE   4
#define TOUCHE_AUTRE	-1

/** Prototypes **/

unsigned char creerFenetre(int largeur,int hauteur,char *titre);
void fermerFenetre(void);
void effacerFenetre(void);
void synchroniserFenetre(void);
void polygonePlein(short int *x,short int *y,int n,int c_int,int c_ext);
void rectanglePlein(int x,int y,int l,int h,int c_int,int c_ext);
void disque(int x,int y,int r,int c_int,int c_ext);
void appliquerDessins(void);
unsigned char attendreEvenement(int *touche,unsigned char *fenetre,unsigned char *quitter);
unsigned char sauverSurface(char *fichier);
unsigned char chargerSurface(char *fichier);
