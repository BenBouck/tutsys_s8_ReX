/******************************************************************/
/* Fichier        : libreseau.h                                   */
/* Emplacement    : libs/reseau/                                  */
/* Auteur         : Léa Viciot                                    */
/* Description    : Fichier d'en-tête contenant les prototypes et */
/*                  les constantes de libreseau.c                 */
/******************************************************************/

/* D'après le cours de X.Redon (https://rex.plil.fr/Enseignement/Reseau/Reseau.IMA4sc/reseau.html) */

#ifndef __LIBRESEAU_H__
#define __LIBRESEAU_H__

/**** Constantes ****/
#define MAX_TCP_MESSAGE 1000

/**** Fonctions ****/

/* Fonction d'initialisation d'un serveur TCP */
int init_serveur_TCP(char *service, int connexions);

/* Fonction de connexion à un serveur TCP */
int connexion_serveur_TCP(char *hote,char *service);

/* Fonction de boucle de serveur TCP générique */
void boucle_serveur_TCP(int socketfd, void(*fonction_boucle)(int));

/* Fonction d'initialisation d'un serveur UDP */
int initialisation_serveur_UDP(char *service);

/* Fonction de récpetion d'un message UDP */
int recevoir_UDP(int s,void* message,int longueur, char* adresseIP, int tailleIP);

/* Fonction de création d'une adresse UDP */
void *creation_addresse_UDP(char *hote, char *service);

/* Fonction d'envoi d'un message UDP */
void envoyer_UDP(int s, void* adresse, unsigned char *message, int taille);

#endif
