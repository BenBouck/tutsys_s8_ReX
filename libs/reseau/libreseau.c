/******************************************************************/
/* Fichier        : libreseau.c                                   */
/* Emplacement    : libs/reseau/                                  */
/* Auteur         : Léa Viciot                                    */
/* Description    : Librairie de gesstion des serveur et clients  */
/*                  TCP et UDP                                    */
/******************************************************************/

/* D'après le cours de X.Redon (https://rex.plil.fr/Enseignement/Reseau/Reseau.IMA4sc/reseau.html) */

/*** Fichiers d'inclusions ***/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <linux/if.h>
#include <linux/if_tun.h>

#include <netinet/tcp.h>

#include <netdb.h>

#include "libreseau.h"

/*** Fonctions ***/

/* Fonction d'initialisation d'un serveur */

int init_serveur_TCP(char *service, int connexions)
{
   struct addrinfo precisions, *resultat, *origine;
   int statut;
   int s;

   /* Construction de la structure adresse */
   
   memset(&precisions,0,sizeof precisions);
   
   precisions.ai_family=AF_UNSPEC;
   precisions.ai_socktype=SOCK_STREAM;
   precisions.ai_flags=AI_PASSIVE;
   
   statut=getaddrinfo(NULL,service,&precisions,&origine);
   
   if(statut<0)
   { 
      perror("init_serveur.getaddrinfo"); 
      exit(EXIT_FAILURE); 
   }
   
   struct addrinfo *p;
   
   for(p=origine,resultat=origine;p!=NULL;p=p->ai_next)
   { 
      if(p->ai_family==AF_INET6)
      {  
         resultat=p; 
         break; 
      } 
   }

   /* Creation d'une socket */
   
   s=socket(resultat->ai_family,resultat->ai_socktype,resultat->ai_protocol);
   
   if(s<0)
   { 
      perror("init_serveur.socket"); 
      exit(EXIT_FAILURE);
   }

   /* Options utiles */
   
   int vrai=1;
   
   if(setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&vrai,sizeof(vrai))<0)
   {
      perror("init_serveur.setsockopt (REUSEADDR)");
      exit(EXIT_FAILURE);
   }
   
   if(setsockopt(s,IPPROTO_TCP,TCP_NODELAY,&vrai,sizeof(vrai))<0)
   {
      perror("init_serveur.setsockopt (NODELAY)");
      exit(EXIT_FAILURE);
   }

   /* Specification de l'adresse de la socket */
   
   statut=bind(s,resultat->ai_addr,resultat->ai_addrlen);
   
   if(statut<0) 
      return -1;

   /* Liberation de la structure d'informations */
   
   freeaddrinfo(origine);

   /* Taille de la queue d'attente */
   
   statut=listen(s,connexions);
   
   if(statut<0) 
      return -1;

   return s;
}

/* Fonction de connexion à un serveur */

int connexion_serveur_TCP(char *hote,char *service)
{
   struct addrinfo precisions,*resultat,*origine;
   int statut;
   int s;

   /* Creation de l'adresse de socket */
   
   memset(&precisions,0,sizeof precisions);
   
   precisions.ai_family=AF_UNSPEC;
   precisions.ai_socktype=SOCK_STREAM;
   
   statut=getaddrinfo(hote,service,&precisions,&origine);
   
   if(statut<0)
   { 
      perror("connexion_serveur.getaddrinfo"); 
      exit(EXIT_FAILURE); 
   }
   
   struct addrinfo *p;
   
   for(p=origine,resultat=origine;p!=NULL;p=p->ai_next)
      if(p->ai_family==AF_INET6)
      { 
         resultat=p; 
	 break; 
      }

   /* Creation d'une socket */
   
   s=socket(resultat->ai_family,resultat->ai_socktype,resultat->ai_protocol);
   
   if(s<0)
   { 
      perror("connexion_serveur.socket"); 
      exit(EXIT_FAILURE); 
   }

   /* Connection de la socket a l'hote */
   if(connect(s,resultat->ai_addr,resultat->ai_addrlen)<0) 
      return -1;

   /* Liberation de la structure d'informations */
   freeaddrinfo(origine);
 
   return s;

}

/* Fonction de boucle serveur */

void boucle_serveur_TCP(int socketfd, void(*fonction_boucle)(int))
{
   int socket_client;

   while(1)
   {
      socket_client = accept(socketfd, NULL, NULL);
      if(socket_client < 0)
      {
         perror("boucle_serveur_TCP.accept");
	 break;
      }

      fonction_boucle(socket_client);
   }
}

/* Fonction pour initialiser un serveur UDP */

int initialisation_serveur_UDP(char *service)
   {
   struct addrinfo precisions,*resultat,*origine;
   int statut;
   int s;

   /* Construction de la structure adresse */
   
   memset(&precisions,0,sizeof precisions);
   
   precisions.ai_family=AF_UNSPEC;
   precisions.ai_socktype=SOCK_DGRAM;
   precisions.ai_flags=AI_PASSIVE;
   
   statut=getaddrinfo(NULL,service,&precisions,&origine);
   
   if(statut<0)
   { 
      perror("initialisationSocketUDP.getaddrinfo"); 
      exit(EXIT_FAILURE); 
   }

   struct addrinfo *p;

   for(p=origine,resultat=origine;p!=NULL;p=p->ai_next)
   
   if(p->ai_family==AF_INET6)
   { 
      resultat=p; 
      break; 
   }

   /* Creation d'une socket */

   s=socket(resultat->ai_family,resultat->ai_socktype,resultat->ai_protocol);
   if(s<0)
   { 
      perror("initialisationSocketUDP.socket"); 
      exit(EXIT_FAILURE); 
   }

   /* Options utiles */
   int vrai=1;
   struct timeval minuteur;

   minuteur.tv_sec=0;
   minuteur.tv_usec=100;

   if(setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&vrai,sizeof vrai)<0)
   {
      perror("initialisationSocketUDP.setsockopt (REUSEADDR)");
      exit(EXIT_FAILURE);
   }

   if(setsockopt(s,SOL_SOCKET,SO_BROADCAST,&vrai,sizeof vrai)<0)
   {
      perror("initialisationSocketUDP.setsockopt (BROADCAST)");
      exit(EXIT_FAILURE);
   }

   if(setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,&minuteur,sizeof minuteur)<0)
   {
      perror("initialisationSocketUDP.setsockopt (BROADCAST)");
      exit(EXIT_FAILURE);
   }

   /* Specification de l'adresse de la socket */
   statut=bind(s,resultat->ai_addr,resultat->ai_addrlen);

   if(statut<0) 
   {
      perror("initialisationSocketUDP.bind");
      close(s); freeaddrinfo(origine);
      return -1;
   }

   /* Liberation de la structure d'informations */
   freeaddrinfo(origine);

   return s;
}

/* Recevoir un message UDP dans les paramètres de sortie, l'adresse est retournée */

int recevoir_UDP(int s,void* message,int longueur, char* adresseIP, int tailleIP)
{
   int taille;
   struct sockaddr_storage adresse;
   socklen_t mtaille=sizeof(struct sockaddr_storage);
   
   int nboctets=recvfrom(s,message,longueur,0,(struct sockaddr*)&adresse,&mtaille);
   
   if(nboctets<0)
      taille=-1; 
   else{
      taille=nboctets;
      getnameinfo((struct sockaddr *)&adresse, mtaille, adresseIP, tailleIP, NULL, 0,0);
   }
   

   return taille; 
}

/* Création d'une adresse socket UDP pour transmission */

void *creation_addresse_UDP(char *hote,char *service)
{
   struct addrinfo precisions,*resultat,*origine;
   int statut;

   /* Creation de l'adresse de socket */
   memset(&precisions,0,sizeof precisions);

   precisions.ai_family=AF_UNSPEC;
   precisions.ai_socktype=SOCK_DGRAM;

   statut=getaddrinfo(hote,service,&precisions,&origine);

   if(statut<0)
   { 
      perror("creationAddressUDP.getaddrinfo"); 
      exit(EXIT_FAILURE);
   }

   struct addrinfo *p;
   for(p=origine,resultat=origine;p!=NULL;p=p->ai_next)
   
   if(p->ai_family==AF_INET6)
   { 
      resultat=p; 
      break; 
   }

   /* Copie de la structure adresse */
   struct sockaddr *adresse=malloc(sizeof(struct sockaddr));

   if(adresse==NULL)
   { 
      perror("creationAddressUDP.malloc"); 
      exit(EXIT_FAILURE); 
   }

   memcpy(adresse,resultat->ai_addr,resultat->ai_addrlen);
   
   /* Liberation de la structure d'informations */
   freeaddrinfo(origine);

   return (void*)adresse;
}

/* Envoyer un message UDP à l'adresse fournie en paramètre (récupérée de la réception de message ou de la création d'adresse) */

void envoyer_UDP(int s, void* adresse, unsigned char *message, int taille)
{
   int nboctets=sendto(s,message,taille,0,(struct sockaddr*)adresse,sizeof(struct sockaddr_storage));

   if(nboctets<0)
   { 
      perror("messageUDP.sento"); 
      exit(EXIT_FAILURE); 
   }
}

