#ifndef __LISTE_CLIENTS_H__
#define __LISTE_CLIENTS_H__

/*** Définition de la liste chaînée des clients ***/
typedef struct liste_clients{
    int socketfd;
    struct liste_clients * suivant;
} liste_clients_t;

/*** Fonctions de traitement de la liste chaînée ***/

/* Fonction d'ajout d'un client */
liste_clients_t * ajouter_client(liste_clients_t * liste, int socketfd);

/* Fonction de recherche d'un client par socketfd 
liste_clients_t recherche_client(liste_clients_t liste, int socketfd); */

/* Fonction de suppression d'un client au socketfd donné */
liste_clients_t * supprimmer_client(liste_clients_t * liste, int socketfd);

/* Fonction de libération de la liste */
void supprimer_liste(liste_clients_t * liste);

#endif