#include <stdlib.h>

#include "libliste_clients.h"

/* Fonction d'ajout d'un client */
liste_clients_t * ajouter_client(liste_clients_t * liste, int socketfd)
{
    if(liste != NULL)
    {
        liste->suivant = ajouter_client(liste->suivant, socketfd);
    }
    else
    {
        liste = malloc(sizeof(liste_clients_t));
        if(liste != NULL)
        {
            liste->socketfd = socketfd;
            liste->suivant = NULL;
        }
    }
    return liste;
}

/* Fonction de recherche d'un client par socketfd *
liste_clients_t * recherche_client(liste_clients_t * liste, int socketfd)
{
    if(liste->socketfd == socketfd)
        return liste;
    else
        if(liste->suivant != NULL)
            return recherche_client(liste->suivant, socketfd);
        else
            return NULL;
}*/

/* Fonction de suppression d'un client au socketfd donné */
liste_clients_t * supprimmer_client(liste_clients_t * liste, int socketfd)
{
    if(liste != NULL)
    {
        if(liste->socketfd == socketfd)
        {
            liste_clients_t *tmp = liste->suivant;
            liste->suivant = NULL;
            liste->socketfd = 0;
            free(liste);
            return tmp;
        }
        else
        {
            liste->suivant = supprimmer_client(liste->suivant, socketfd);
            return liste;
        }
    }
    return NULL;
}

/* Fonction de libération de la liste */
void supprimer_liste(liste_clients_t * liste)
{
    if(liste != NULL)
    {
        supprimer_liste(liste->suivant);
        liste->suivant = NULL;
        liste->socketfd = 0;
        free(liste);
    }
}