/******************************************************************/
/* Fichier        : libreseau.h                                   */
/* Emplacement    : libs/reseau/                                  */
/* Auteur         : BOUCKART Benoit - VICIOT Léa                  */
/* Description    : Fichier contenant les fonctions de flux       */
/*                                                                */
/******************************************************************/

/* D'après le cours de X.Redon (https://rex.plil.fr/Enseignement/Reseau/Reseau.IMA4sc/reseau.html) */

/*** Fichiers d'inclusions ***/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include "libflux.h"

/*** Variables ***/
pthread_mutex_t semaphores[NB_MUTEX];

/*** Fonctions ***/

/* Fonction de création de thread générique */
void creer_thread(void(*fonction)(void*), void* argument, size_t taille_arg)
{
    fonction_thread_t* fnt = NULL;

    //Malloc de fnt :
    fnt = malloc(sizeof(fonction_thread_t));
    if (fnt == NULL)
        exit(-1);

    //Fnt -> Fonction
    fnt -> fonction = fonction;

    //Fnt -> Argument : Malloc pour les arguments, puis assignation de la valeur de l'argument
    fnt -> adresse_arg = malloc(taille_arg);

    //Copie la zone mémoire
    memcpy(fnt -> adresse_arg, argument, taille_arg);

    //Création du pthread
    pthread_t tid;
    pthread_create(&tid, NULL, fonction_thread, fnt);
    pthread_detach(tid);
}

/* Fonction générique passée à pthread */
void* fonction_thread(void* struct_fn)
{
    fonction_thread_t *f	    = struct_fn;

    /* Appel de la fonction */
    f->fonction(f->adresse_arg);

    /* Libération */
    if(f->adresse_arg != NULL)
    	free(f->adresse_arg);
    free(f);

    return NULL;
}

/* Fonction générique pour initialiser les sémaphores */
/* Tester la valeur de retour dans l'exécutable */
int init_semaphore(int num_sem) 
{
    //Initialisation + Récupération de la valeur par la variable statut
    return pthread_mutex_init (semaphores + num_sem*sizeof(pthread_mutex_t), NULL);
}

/* Fonction qui detruit la sémaphore */
int semaphore_destroy(int num_sem)
{
    return pthread_mutex_destroy(semaphores + num_sem*sizeof(pthread_mutex_t));
}

/* Fonction générique pour la sémaphore : "Verouille" */
int semaphore_lock(int num_sem)
{
    return pthread_mutex_lock(semaphores + num_sem*sizeof(pthread_mutex_t));
}

/* Fonction générique pour la sémaphore : "Dévérouille */
int semaphore_unlock(int num_sem)
{
    return pthread_mutex_unlock(semaphores + num_sem*sizeof(pthread_mutex_t));
}

