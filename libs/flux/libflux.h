/******************************************************************/
/* Fichier        : libflux.c                                     */
/* Emplacement    : libs/reseau/                                  */
/* Auteur         : BOUCKAERT Benoît - VICIOT Léa                 */
/* Description    : Librairie de gesstion des threads sans passer */
/*                  par l'interface pthread                       */
/******************************************************************/

#include <stdlib.h>

#ifndef __LIBFLUX_H__
#define __LIBFLUX_H__

/*** Constantes ***/
#define NB_MUTEX 100 //Serveur chat + joueurs

/*** Structure de fonction ***/

typedef struct fonction_thread{
    void    (*fonction)(void*);
    void*   adresse_arg;
} fonction_thread_t;

/*** Déclaration des fonctions ***/

/* Fonction de création de thread générique */
void creer_thread(void(*fonction)(void*), void* argument, size_t taille_arg);

/* Fonction générique passée à pthread */
void* fonction_thread(void* struct_fn);

/* Fonction générique pour initialiser les sémaphores */
int init_semaphore(int num_sem);

/* Fonction qui detruit la sémaphore */
int semaphore_destroy(int num_sem);

/* Fonction générique pour la sémaphore : "Verouille" */
int semaphore_lock(int num_sem);

/* Fonction générique pour la sémaphore : "Dévérouille */
int semaphore_unlock(int num_sem);



#endif
