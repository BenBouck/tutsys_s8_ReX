/******************************************************************/
/* Fichier        : client.c                                      */
/* Emplacement    : Src_client                                    */
/* Auteur         : BOUCKAERT Benoit - VICIOT Léa                 */
/* Description    : Exécutable d'un client chat                   */
/*                                                                */
/******************************************************************/

/*** Fichier d'inclusions ***/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libreseau.h"
#include "libflux.h"
#include "libliste_clients.h"
#include "protocole_perso.h"

/*** Constantes ***/

/*** Variables globales ***/

/*** Fonctions ***/

void attend_UDP(int s, char* adIp)
{    
    char message[LM_MESSAGE];
    
    while((recevoir_UDP(s, message, LM_MESSAGE, adIp, 128))<0);

    printf("Connexion au serveur %s@%s\n", message, adIp);
   
    /* Fermeture de la socket de dialogue */
    close(s);
}

void ecoute_serveur(void* arg)
{
    int s = *(int*)arg;
    char message[LM_MESSAGE];
    int taille=1;

    while ((taille=read(s, message, LM_MESSAGE)) > 0)
        write(1, message, taille);
    
    printf("Serveur KO\n");
    exit(EXIT_SUCCESS);
}

void ecoute_clavier(int s)
{
    char message[LM_MESSAGE];
    int taille;

    do
    {
        fgets(message, LM_MESSAGE, stdin);
        taille = strlen(message);
        write(s, message, taille);
    } while (taille>0);
}

void analyse_arguments(int argc, char* argv[], char* pseudo)
{
    if(argc == 2)
    {
        strcpy(pseudo, argv[1]);
    }
    else
    {
        perror("Erreur : nombre d'arguments invalide.\nUtilisation : client <pseudonyme>\n");
        exit(EXIT_SUCCESS);
    }
}

/*** Fonction principale ***/
int main(int argc, char* argv[])
{
    int s;
    char adIp[128];
    char pseudo[LM_PSEUDO];

    analyse_arguments(argc, argv, pseudo);

    /* Initialiser un serveur UDP */
    s = initialisation_serveur_UDP(P_UDP_BALISE);

    /* Dialogue avec le serveur */
    attend_UDP(s, adIp);

    s = connexion_serveur_TCP(adIp, P_TCP_CHAT);
    write(s, pseudo, strlen(pseudo));

    creer_thread(ecoute_serveur, (void*)&s, sizeof(s));
    ecoute_clavier(s);
    
    return 0;
}