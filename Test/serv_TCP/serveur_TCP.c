/******************************************************************/
/* Fichier        : libreseau.c                                   */
/* Emplacement    : libs/reseau/                                  */
/* Auteur         : BOUCKAERT Benoit - VICIOT Léa                 */
/* Description    : Exécutable du serveur TCP - Teste les         */
/*                  fonctions de la libreseau.a                   */
/******************************************************************/

/*** Fichier d'inclusions ***/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libreseau.h"
#include "libflux.h"
#include "libliste_clients.h"

/*** Constantes ***/

#define LEN_PSEUDO         10

#define LOCK_LISTE_CLIENTS 0
#define LOCK_ENVOI_MESSAGE 1

/*** Variables globales ***/

liste_clients_t * liste_clients;

/*** Fonctions ***/

void envoi_message_tous(char* pseudo, char* message, liste_clients_t * liste, int envoyeur)
{
    liste_clients_t* _liste = liste;

    char _message[MAX_TCP_MESSAGE];
            
    memset(_message, 0, MAX_TCP_MESSAGE);
    strcat(_message, pseudo);
    strcat(_message, " : ");
    strcat(_message, message);
    printf("%s", _message);

    semaphore_lock(LOCK_LISTE_CLIENTS);
    semaphore_lock(LOCK_ENVOI_MESSAGE);

    while(_liste != NULL)
    {
        if(_liste->socketfd != envoyeur)
        {
            write(_liste->socketfd, (void*)_message, MAX_TCP_MESSAGE);
        }
        _liste = _liste->suivant;
    }

    semaphore_unlock(LOCK_LISTE_CLIENTS);
    semaphore_unlock(LOCK_ENVOI_MESSAGE);
}

void thread_client(void* arg)
{
    int     client = *(int*)arg;
    char    message[MAX_TCP_MESSAGE];
    char    pseudo[LEN_PSEUDO];
    int     len;

    semaphore_lock(LOCK_LISTE_CLIENTS);
    liste_clients = ajouter_client(liste_clients, client);
    semaphore_unlock(LOCK_LISTE_CLIENTS);

    len = read(client, pseudo, LEN_PSEUDO);

    do
    {
        memset(message, 0, MAX_TCP_MESSAGE);
        len = read(client, (void*)message, MAX_TCP_MESSAGE);
        
        if(len < 0)
        {
            perror("thread_client.read");
            exit(EXIT_FAILURE);
        }

        if(len != 0)
            envoi_message_tous(pseudo, message, liste_clients, client);


    } while(len != 0);

    semaphore_lock(LOCK_LISTE_CLIENTS);
    liste_clients = supprimmer_client(liste_clients, client);
    semaphore_unlock(LOCK_LISTE_CLIENTS);

    envoi_message_tous(pseudo, "déconnecté.\n", liste_clients, 0);
}

void traitement_client(int id_client)
{
   creer_thread(thread_client, (void*)&id_client, sizeof(id_client));
}

/* Fonction principale */

int main ()
{
    int id_socket;
    int statut;

    /** Création des sémaphores **/
    statut = init_semaphore(LOCK_LISTE_CLIENTS);
    if(statut<0)
    {
        perror("main.init_semaphore (LOCK_LISTE_CLIENTS)");
        exit(EXIT_FAILURE);
    }

    statut = init_semaphore(LOCK_ENVOI_MESSAGE);
    if(statut<0)
    {
        perror("main.init_semaphore (LOCK_ENVOI_MESSAGE)");
        exit(EXIT_FAILURE);
    }

    /** Initialisation du serveur **/
    id_socket = init_serveur_TCP("55", 1); //Initialise le serveur sur le port 55 de la carte réseau

    if (id_socket < 0)
    {
        perror("main.init_serveur");
        exit(EXIT_FAILURE);
    }

    /** Appel de la fonction boucle serveur **/
    boucle_serveur_TCP(id_socket, traitement_client);
    
    return 0;
}
