#include <stdlib.h>
#include <math.h>

#include "projecteur.h"
#include "fenetre.h"

/*** Macro degré->radian ***/

#define DEGRAD(x) ((float)2*M_PI*(x)/360)

/*** Soustraction de points ***/

Coordonnees _soustraire_points(Coordonnees p1, Coordonnees p2)
{
    Coordonnees p;
    p.x=p1.x-p2.x;
    p.y=p1.y-p2.y;
    return p;
}


/*** Translation des objets ***/

/** Décalage **/

void _decale_murs(Mur *mur,Coordonnees position)
{
    *mur->points[0] = _soustraire_points(*mur->points[0], position);
    *mur->points[1] = _soustraire_points(*mur->points[1], position);
}

void _decale_entites(Entite* e, Coordonnees position)
{
    *e->pos = _soustraire_points(*e->pos, position);
}

/** Rotation **/

void _rotation_murs(Mur* mur, float angle_rad)
{
    int i, x, y;
    for(i=0;i<2;i++)
    {
        x= - mur->points[i]->x * sin(angle_rad) + mur->points[i]->y * cos(angle_rad);
        y= mur->points[i]->x * cos(angle_rad) + mur->points[i]->y * sin(angle_rad);
        mur->points[i]->x = x;
        mur->points[i]->y = y;
    }
}

void _rotation_entites(Entite* e, float angle_rad)
{
    int x = - e->pos->x * sin(angle_rad) + e->pos->y * cos(angle_rad);
    int y = e->pos->x * cos(angle_rad) + e->pos->y * sin(angle_rad);
    e->pos->x = x;
    e->pos->y = y;
}

/** Translation **/

void translate_entites(Liste_entites* entites, Entite joueur)
{
    float angle = DEGRAD(joueur.orientation);
    if(entites != NULL)
    {
        _decale_entites(entites->entite, *joueur.pos);
        _rotation_entites(entites->entite, angle);
        translate_entites(entites->suivant, joueur);
    }
}

void translate_murs(Liste_murs* murs, Entite joueur)
{
    float angle = DEGRAD(joueur.orientation);
    if(murs != NULL)
    {
        _decale_murs(murs->mur, *joueur.pos);
        _rotation_murs(murs->mur, angle);
        translate_murs(murs->suivant, joueur);
    }
}

/* Projection en 2D */

int _projette_mur(Mur mur, Objet2D* obj)
{
  int x1=mur.points[0]->x; 
  int y1=mur.points[0]->y;
  int x2=mur.points[1]->x; 
  int y2=mur.points[1]->y;

  /* Si les points ne sont pas derrière le joueur */
  if(!(y1<=0 && y2<=0))
  {
    if(y1<=0)
    {
      mur.points[0]->x = x2+(x1-x2)*(1-y2)/(y1-y2); 
      mur.points[0]->y = 1;             
    }
    if(y2<=0)
    {
      mur.points[1]->x = x1+(x2-x1)*(1-y1)/(y2-y1); 
      mur.points[1]->y = 1;
    }
    (*obj).type=TYPE_MUR_2D;
    for(int j=0;j<4;j++)
    {
      int z=mur.points[j/2]->y;
      int x=mur.points[j/2]->x;
      int y=MUR_HAUTEUR;
      int px,py;
      px=LARGEUR_FENETRE/2+x*FOCALE/z;
      py=HAUTEUR_FENETRE/2+((j==0||j==3)?1:-1)*(y-HAUTEUR_FENETRE/4)*FOCALE/z;
      (*obj).def.p[j].x=px;
      (*obj).def.p[j].y=py;
    }
    return 1;
  }
  else
    return 0;
}

int _projette_entite(Entite ent, Objet2D *obj)
{
  if(ent.pos->y > 0)
  {
    (*obj).type = TYPE_CER_2D;
    int x = ent.pos->x;
    int z = ent.pos->y;
    (*obj).def.c.centre.x = LARGEUR_FENETRE/2+x*FOCALE/z;
    (*obj).def.c.centre.y = HAUTEUR_FENETRE/2;
    (*obj).def.c.rayon = ent.rayon*HAUTEUR_FENETRE/(3*MUR_HAUTEUR)*FOCALE/z;
    return 1;
  }
  else
    return 0;
}

void projette_objets(Liste_objets *objet, Objet2D *objets, int* no)
{
  if(objet != NULL)  
  {
    switch (objet->o.type)
    {
    case TYPE_MUR:
      *no += _projette_mur(objet->o.m, &objets[*no]);
      break;
    case TYPE_ENTITE:
      *no += _projette_entite(objet->o.e, &objets[*no]);
      break;
    default:
      break;
    }
    projette_objets(objet->suivant, objets, no);
  }
}