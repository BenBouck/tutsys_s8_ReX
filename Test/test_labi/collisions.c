#include "collisions.h"
#include "hitboxes.h"
#include <stdlib.h>

unsigned char verif_collisions(Entite perso, Liste_entites* entites)
{
    if(entites != NULL)
    {
        if (perso.id_ent != entites->entite->id_ent && collision_ent(perso, *entites->entite))
            return COLLISION_ENTITE;
        else
            return verif_collisions(perso, entites->suivant);
    }
    else
        return PAS_DE_COLLISION;
}