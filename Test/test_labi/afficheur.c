#include "afficheur.h"
#include "libgraph.h"

/** Macros **/
#define sign(a)	(((a)==0)?0:(((a)>0)?1:-1))

/* Intersection d'un segment */

unsigned char _inter_seg_v(Point2D a,Point2D b,int x,int *y)
{
  if(sign(a.x-x)==sign(b.x-x)) 
    return 0;

  *y=a.y+(b.y-a.y)*(x-a.x)/(b.x-a.x);
  return 1;
}

unsigned char _inter_seg_h(Point2D a,Point2D b,int *x,int y)
{
  if(sign(a.y-y)==sign(b.y-y)) 
    return 0;
  
  *x=a.x+(b.x-a.x)*(y-a.y)/(b.y-a.y);
  return 1;
}

/* Intersection d'un polygone avec un rectangle */

void _inter_poly_rect(Point2D *orig,int no,Point2D *result,int *nr)
{
  Point2D avant[POINTS_MAX];
  Point2D apres[POINTS_MAX];
  int i,j;
  for(i=0;i<no;i++) avant[i]=orig[i];
  int nv=no;
  for(i=0;i<4;i++)
  {
    int np=0;
    for(j=0;j<nv;j++)
    {
      int p=(j+nv-1)%nv;
      Point2D a=avant[p];
      Point2D b=avant[j];
      int x,y;
      unsigned char inta,intb;
      switch(i)
      {
        case 0:
          x=0;
          _inter_seg_v(a,b,x,&y);
          if(a.x>=0) inta=1; else inta=0;
          if(b.x>=0) intb=1; else intb=0;
          break;

        case 1:
          y=HAUTEUR_FENETRE;
          _inter_seg_h(a,b,&x,y);
          if(a.y<=HAUTEUR_FENETRE) inta=1; else inta=0;
          if(b.y<=HAUTEUR_FENETRE) intb=1; else intb=0;
          break;

        case 2:
          x=LARGEUR_FENETRE;
          _inter_seg_v(a,b,x,&y);
          if(a.x<=LARGEUR_FENETRE) inta=1; else inta=0;
          if(b.x<=LARGEUR_FENETRE) intb=1; else intb=0;
          break;

        case 3:
          y=0;
          _inter_seg_h(a,b,&x,y);
          if(a.y>=0) inta=1; else inta=0;
          if(b.y>=0) intb=1; else intb=0;
          break;
      }

      if(intb)
      {
        if(!inta){ apres[np].x=x; apres[np].y=y; np++; }
        apres[np++]=b;
      }

      else
      {
        if(inta)
        { 
          apres[np].x=x; 
          apres[np].y=y; 
          np++; 
        }
      }
    }

    for(j=0;j<np;j++) 
      avant[j]=apres[j];

    nv=np;
  }

  for(i=0;i<nv;i++) 
    result[i]=avant[i];
  
  *nr=nv;
}

/* Dessin d'un labyrinthe */

void dessine_2D(Objet2D *objet,int no)
{
  int i,j;
  short int x[POINTS_MAX];
  short int y[POINTS_MAX];
  for(i=0;i<no;i++)
  {
    if(objet[i].type==TYPE_MUR_2D)
    {
      Point2D poly[POINTS_MAX];
      int np;
      _inter_poly_rect(objet[i].def.p,4,poly,&np);
      
      for(j=0;j<np;j++)
      {
        x[j]=poly[j].x;
        y[j]=HAUTEUR_FENETRE-poly[j].y;
      }
      polygonePlein(x,y,np,COULEUR_ROUGE,COULEUR_ROSE);
    }
    if(objet[i].type==TYPE_CER_2D)
    {
      disque(objet[i].def.c.centre.x, objet[i].def.c.centre.y, objet[i].def.c.rayon, COULEUR_JAUNE, COULEUR_BLANC);
    }
  }
}
