#include "mouvement.h"

#include <stdlib.h>
#include <math.h>

/*** Fonctions ***/

/** Détection de toutes les collisions (privée) **/

/* Collision entite-liste entites */
unsigned char _det_coll_e(Entite e, Liste_entites* l)
{
    unsigned char ret;

    if(l != NULL)
    {
        if(collision_ent(e, *l->entite) && e.id_ent != l->entite->id_ent)
        {
            ret = COLLISION_ENTITE;
        }
        else
        {
            ret = _det_coll_e(e, l->suivant);
        }
    }
    else
    {
        ret = PAS_DE_COLLISION;
    }

    return ret;
}

/* Collision entite-liste murs */
unsigned char _det_coll_m(Entite e, Liste_murs* m)
{
    unsigned char ret;

    if(m != NULL)
    {
        if(collision_mur(e, *m->mur))
        {
            ret = COLLISION_MUR;
        }
        else
        {
            ret = _det_coll_m(e, m->suivant);
        }
    }
    else
    {
        ret = PAS_DE_COLLISION;
    }

    return ret;
}

/* Fonction appelant les 3 */
unsigned char _det_tt_col(Entite e, Liste_entites* j, Liste_entites* t, Liste_murs* m)
{
    unsigned char ret;
    
    if((ret = _det_coll_e(e, j)) != PAS_DE_COLLISION)
        return ret;

    if((ret = _det_coll_e(e, t)) != PAS_DE_COLLISION)
        return ret;

    if((ret = _det_coll_m(e, m)) != PAS_DE_COLLISION)
        return ret;

    return PAS_DE_COLLISION;
}

/** Déplacement d'une entité (privée) **/
unsigned char _dep_ent(Entite* e, int v, Liste_entites* j, Liste_entites* t, Liste_murs* m)
{
    int pos_old, retx, rety;

    pos_old = e->pos->x;
    e->pos->x += v*cos(M_PI * 2 * (double)e->orientation / 360);
    if((retx = _det_tt_col(*e, j, t, m)) != PAS_DE_COLLISION)
        e->pos->x = pos_old;
    
    pos_old = e->pos->y;
    e->pos->y += v*sin(M_PI * 2 * (double)e->orientation / 360);
    if((rety = _det_tt_col(*e, j, t, m)) != PAS_DE_COLLISION)
        e->pos->y = pos_old;

    if(retx==COLLISION_ENTITE||rety==COLLISION_ENTITE)
        return COLLISION_ENTITE;
    else if(retx==COLLISION_MUR||rety==COLLISION_MUR)
        return COLLISION_MUR;
    else
        return PAS_DE_COLLISION;
}

/** Déplacement d'un joueur **/
/* Retours :
 * - 0 si pas de collision avec un joueur/tir
 * - 1 si collision avec un joueur/tir */

/* Déplacement vers l'avant */
unsigned char deplacement_avant(Entite* joueur, Liste_entites* joueurs, Liste_entites* tirs, Liste_murs* murs)
{
    return _dep_ent(joueur, VITESSE_JOUEUR, joueurs, tirs, murs);
}

/* Déplacement vers l'arrière */
unsigned char deplacement_arriere(Entite* joueur, Liste_entites* joueurs, Liste_entites* tirs, Liste_murs* murs)
{
    return _dep_ent(joueur, -VITESSE_JOUEUR, joueurs, tirs, murs);
}

/** Déplacement d'un tir **/
unsigned char deplacement_tir(Entite* tir, Liste_murs* murs)
{
    int ret;
    for(int i = 0;i<VITESSE_TIR;i+=tir->rayon)
        if((ret=_dep_ent(tir, tir->rayon, NULL, NULL, murs))!=PAS_DE_COLLISION)
            break;
    return ret;
}
