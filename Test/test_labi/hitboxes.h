#ifndef __HITBOXES_H__
#define __HITBOXES_H__

#include "entites.h"

/*** Constantes ***/

#define RAYON_JOUEUR 50
#define RAYON_BALLE 5

/*** Fonctions ***/

/** Détection de collisions **/

/* Collision entre entités */
unsigned char collision_ent(Entite a, Entite b);

/* Collision avec un mur */
unsigned char collision_mur(Entite e, Mur m);

#endif