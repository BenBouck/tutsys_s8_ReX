#ifndef __AFFICHEUR_H__
#define __AFFICHEUR_H__

#include "fenetre.h"
#include "objets2D.h"

#define POINTS_MAX	32

void dessine_2D(Objet2D *objet,int no);

#endif