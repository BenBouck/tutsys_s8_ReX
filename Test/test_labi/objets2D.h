#ifndef __OBJETS2D_H__
#define __OBJETS2D_H__

/*** Structures ***/
typedef struct
{
  int x,y;
} Point2D;

typedef struct
{
  Point2D centre;
  int rayon;
} Cercle2D;

typedef struct
{
  int type;
  union 
  {
    Point2D p[4];
    Cercle2D c;
  } def;
} Objet2D;

/*** Constantes ***/
#define	TYPE_MUR_2D 0
#define TYPE_CER_2D 1

#endif