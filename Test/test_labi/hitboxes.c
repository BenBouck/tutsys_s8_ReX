#include <math.h>
#include <stdio.h>

#include "hitboxes.h"

#define MAX(A, B) (((A) > (B)) ? (A) : (B))
#define MIN(A, B) (((A) < (B)) ? (A) : (B))

/* Fonction de déteciton de collision entre entités
 *  - Si collision : retour de 1
 *  - Sinon        : retour de 0
 */
unsigned char collision_ent(Entite a, Entite b)
{
    unsigned int dist_x;
    unsigned int dist_y;
    unsigned int distance;

    if(a.id_ent == b.id_ent)
        return 0;

    /* Calcul des distances projetées sur les axes x et y */
    dist_x = a.pos->x - b.pos->x;
    dist_y = a.pos->y - b.pos->y;

    /* Calcul de la distance */
    distance = dist_x*dist_x + dist_y*dist_y;

    /* Résultat : si la distance des centres est inférieure
     *  ou égale à la somme des rayons, il y a collision */
    return (distance <= (a.rayon+b.rayon)*(a.rayon+b.rayon))?1:0;
}

/* Fonction de détection de collision entre un mur et une entitée
 *  - Si collision : retour de 1
 *  - Sinon        : retour de 0
 */
unsigned char collision_mur(Entite e, Mur m)
{   
    int resultat = 0;
    /* Si le mur est le long de l'axe y */
    if(m.points[0]->y == m.points[1]->y)
    {
        /* On vérifie si l'entité peut le toucher sur l'axe y */
        int r = e.rayon;
        int y = m.points[0]->y;
        int yc = e.pos->y;
        if(((yc + r) > y && yc < y) || ((yc - r) < y && yc > y))
        {
            /* On vérifie si l'entité peut le toucher sur l'axe x */
            int x0 = MIN(m.points[0]->x, m.points[1]->x);
            int x1 = MAX(m.points[0]->x, m.points[1]->x);
            int xc = e.pos->x;
            if((xc+r)>x0 && (xc-r)<x1)
                resultat = 1;
        }
    }
    else
    {
        /* On vérifie si l'entité peut le toucher sur l'axe x */
        int r = e.rayon;
        int x = m.points[0]->x;
        int xc = e.pos->x;
        if(((xc + r) > x && xc < x) || ((xc - r) < x && xc > x))
        {
            /* On vérifie que l'entité peut le toucher sur l'axe y */
            int y0 = MIN(m.points[0]->y, m.points[1]->y);
            int y1 = MAX(m.points[0]->y, m.points[1]->y);
            int yc = e.pos->y;
            if((yc+r)>y0 && (yc-r)<y1)
                resultat = 1;
        }
    }

    /* Fonction simplifiable en un simple return */
    return resultat;
}