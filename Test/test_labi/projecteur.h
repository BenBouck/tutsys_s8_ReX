#ifndef __PROJECTEUR_H__
#define __PROJECTEUR_H__

/*** Inclusions ***/
#include "entites.h"
#include "objets2D.h"

/*** Constantes ***/
#define MUR_HAUTEUR 200
#define FOCALE (3*MUR_HAUTEUR)

/*** Fonctions ***/
void translate_entites(Liste_entites* entites, Entite joueur);
void translate_murs(Liste_murs* murs, Entite joueur);
void projette_objets(Liste_objets* objets3D, Objet2D* objets2D, int* nb);

#endif