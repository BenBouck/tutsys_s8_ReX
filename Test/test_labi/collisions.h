#ifndef __COLLISIONS_H__
#define __COLLISIONS_H__

#include "mouvement.h"
#include "entites.h"

unsigned char verif_collisions(Entite perso, Liste_entites* entites);

#endif