#ifndef __MOUVEMENT_H__
#define __MOUVEMENT_H__

/*** Inclusions ***/
#include "entites.h"
#include "hitboxes.h"

/*** Constantes ***/

/** Vitesses de déplacement **/
#define VITESSE_JOUEUR  10
#define VITESSE_TIR     20

/** Retours **/
#define PAS_DE_COLLISION    0
#define COLLISION_MUR       1
#define COLLISION_ENTITE    2

/*** Fonctions ***/

/** Déplacement d'un joueur **/

/* Déplacement vers l'avant */
unsigned char deplacement_avant(Entite* joueur, Liste_entites* joueurs, Liste_entites* tirs, Liste_murs* murs);

/* Déplacement vers l'arrière */
unsigned char deplacement_arriere(Entite* joueur, Liste_entites* joueurs, Liste_entites* tirs, Liste_murs* murs);

/** Déplacement d'un tir **/
unsigned char deplacement_tir(Entite* tir, Liste_murs* murs);

#endif