#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>

#include "moteur_jeu.h"
#include "libflux.h"
#include "libgraph.h"
#include "entites.h"
#include "projecteur.h"
#include "hitboxes.h"
#include "mouvement.h"
#include "afficheur.h"
#include "collisions.h"

/** Variables globales **/

int touches;
unsigned char quitter;
int fin;

void ecoute_touches(void* arg)
{
    unsigned char fenetre;
    quitter=0;
    fin=0;
    while(quitter==0 && fin == 0)
        attendreEvenement(&touches, &fenetre, &quitter);
}

void rendu_graphique(Entite joueur, Liste_entites* joueurs, Liste_entites* tirs, Liste_murs* murs)
{
    Liste_murs* murs2 = NULL;
    Objet2D *objets = NULL;
    Liste_objets* objetsliste = NULL;
    Liste_entites* joueurs2 = NULL;
    Liste_entites* tirs2 = tirs;
    int no = 0;

    //printf("x:%d ; y:%d ; facing:%d\n", p->pos->x, p->pos->y, p->orientation);

    /* Duplique la liste de murs et les déplace en fonction de la position du joueur */
    murs2 = duplique_liste_murs(murs);
    tirs2 = duplique_liste_entites(tirs);
    joueurs2 = duplique_liste_entites(joueurs);

    translate_murs(murs2, joueur);
    translate_entites(tirs2, joueur);
    translate_entites(joueurs2, joueur);

    /* Ajoute les objets déplacés dans une liste unique */
    objetsliste = ajouter_murs(objetsliste, murs2);
    objetsliste = ajouter_entites(objetsliste, joueurs2);
    objetsliste = ajouter_entites(objetsliste, tirs2);
    trie_liste_objets(objetsliste);

    /* Allocation surdimentionnée (pour les tests) */
    objets=malloc(1000*sizeof(Objet2D));

    /* Projection en premiere personne des objets 2D */
    projette_objets(objetsliste, objets, &no);

    /* Vide la liste dupliquée */
    vider_liste_objets(objetsliste);
    vider_liste_entites(joueurs2);
    vider_liste_murs(murs2);
    vider_liste_entites(tirs2);

    /* Affichage des objets */
    effacerFenetre(); 
    dessine_2D(objets,no);
    free(objets);
    synchroniserFenetre();
}


int jeu(Liste_murs* murs)
{
    /* Initialisation */
    Liste_entites*  joueurs = NULL;
    Liste_entites*  tirs    = NULL; 

    Entite*         p       = new_entite(ID_PLAYER, LABY_X/2*MUR_HAUTEUR,MUR_HAUTEUR, RAYON_JOUEUR, 0);
    Entite*         mob     = new_entite(ID_MOB, 1200, 800, RAYON_JOUEUR, 0);

    int cooldown_tir_joueur = 0;
    int cooldown_tir_mob = 1;
    unsigned char comportement_mob;
    int type_fin = JEU_QUITTE;

    joueurs = ajouter_entite(joueurs, p);
    joueurs = ajouter_entite(joueurs, mob);

    /* Ecoute multi-touches */
    creer_thread(ecoute_touches, NULL, 0);

    srand(time(NULL));
    
    /* Boucle */
    while(1)
    {        
        usleep(ATTENTE);

        /* Appui d'une touche */
        if(touches != 0)
        {
            /* Tour sur soi-même si touches de côté */
            if(touches & (1 << TOUCHE_DROITE))
                p->orientation += (p->orientation >= 360)?-355:2;
            if(touches & (1 << TOUCHE_GAUCHE)) 
                p->orientation -= (p->orientation <= 0)?-355:2;

            /* Déplacement vers l'avant */
            if(touches & (1 << TOUCHE_HAUT))
                deplacement_avant(p, joueurs, tirs, murs);

            /* Déplacement vers l'arrière */
            if(touches & (1 << TOUCHE_BAS))
                deplacement_arriere(p, joueurs, tirs, murs);

            /* Tir sur pression de Espace */
            if((touches & (1 << TOUCHE_ESPACE)) && (cooldown_tir_joueur == 0))
            {
                Entite* tir = new_entite(ID_PLAYER, p->pos->x, p->pos->y, RAYON_BALLE, p->orientation);
                tirs = ajouter_entite(tirs, tir);
                cooldown_tir_joueur = 1000000/(ATTENTE*CADENCE);
            }
        }

        /* Fermeture de la fenêtre */
        if(quitter==1)
            break;

        /* Comportement aléatoire pour le mob */
        comportement_mob = rand() % 100;
        if(comportement_mob < P_AVANCER)
            deplacement_avant(mob, joueurs, tirs, murs);
        else
        {
            comportement_mob -= P_AVANCER;
            if(comportement_mob < P_GAUCHE)
                mob->orientation -= (mob->orientation <= 0)?-355:2;
            else
            {
                comportement_mob -= P_GAUCHE;
                if(comportement_mob < P_DROITE)
                    mob->orientation += (mob->orientation <= 0)?-355:2;
                else
                {
                    comportement_mob -= P_DROITE;
                    if(comportement_mob < P_RECULER)
                        deplacement_arriere(mob, joueurs, tirs, murs);
                    else if(cooldown_tir_mob == 0)
                    {
                        Entite* tir = new_entite(ID_MOB, mob->pos->x, mob->pos->y, RAYON_BALLE, mob->orientation);
                        tirs = ajouter_entite(tirs, tir);
                        cooldown_tir_mob = 1000000/(ATTENTE*CADENCE);
                    }
                }
            }
        }

        /* Déplace les tir et vérifie la collision */
        Liste_entites* tirs_tmp = tirs;
        while (tirs_tmp!=NULL)
        {
            if(deplacement_tir(tirs_tmp->entite, murs) == COLLISION_MUR)
            {
                Liste_entites* tmp = tirs_tmp->suivant;
                tirs = enlever_entite(tirs, tirs_tmp->entite);
                tirs_tmp = tmp;
            }
            else
                tirs_tmp = tirs_tmp->suivant;
        }

        /* Gestion de la recharge des tirs */
        if(cooldown_tir_joueur != 0)
            cooldown_tir_joueur --;

        if(cooldown_tir_mob != 0)
            cooldown_tir_mob --;

        /* Test des impacts */
        if(verif_collisions(*mob, tirs) == COLLISION_ENTITE)
        {
            type_fin = JEU_GAGNE;
            fin = 1;
            break;
        }
        else if (verif_collisions(*p, tirs) == COLLISION_ENTITE)
        {
            type_fin = JEU_PERDU;
            fin = 1;
            break;
        }

        rendu_graphique(*p, joueurs, tirs, murs);
    }    
    vider_liste_entites(joueurs);
    vider_liste_entites(tirs);
    vider_liste_murs(murs);

    return type_fin;
}