/******************************************************************/
/* Fichier        : clien_chat.c                                  */
/* Emplacement    : Test                                          */
/* Auteur         : BOUCKAERT Benoit - VICIOT Léa                 */
/* Description    : Exécutable d'un client chat                   */
/*                                                                */
/******************************************************************/

/*** Fichier d'inclusions ***/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libreseau.h"
#include "libflux.h"
#include "libliste_clients.h"

/*** Variables globales ***/
int s; //Descripteur de socket

/*** Fonction ***/
void analyse_arguments(int argc, char *argv[])
{
    if(argc != 4)
    {
        perror("Erreur de synthaxe\nExemple : client_chat <adresse ip du serveur> <port> <pseudo du joueur>");
        exit(EXIT_SUCCESS); //Programme fonctionne, erreur de synthaxe
    }
}

void envoie_message (void *arg)
{
    char message[MAX_TCP_MESSAGE];
    int len;
    do 
    {
        /* Lit sur le clavier */
        len = read(STDIN_FILENO, message, MAX_TCP_MESSAGE);

        if(len < 0)
        {
            perror("Erreur envoie message \n");
            exit(EXIT_FAILURE);
        }

        /*Ecrit sur le serveur */
        len = write(s, message, len);

    } while (len > 0);
}

void recoit_message(void *arg)
{
    char message[MAX_TCP_MESSAGE];
    int len;

    do
    {
        /* Lit sur le serveur */
        len = read(s, message, MAX_TCP_MESSAGE);
        if(len < 0)
        {
            perror("Erreur recevoir message \n");
            exit(EXIT_FAILURE);
        }

        write(1, message, len);

    } while(len != 0);

}

/*** Fonction principale ***/

int main(int argc, char *argv[])
{

    /* Fonction analyse arguments argc et argv */
    analyse_arguments(argc, argv);
    
    /* Fonction de connexion au serveur*/
    s = connexion_serveur_TCP(argv[1],argv[2]);

	char *message=argv[3];
    write(s, message, strlen(message)); //On envoie le pseudo du joueur

    /* Création de deux threads */
    creer_thread(envoie_message,NULL, 0); //Thread envoie_message
    creer_thread(recoit_message,NULL, 0); //Thread reçoit message

    while(1);
}
