# Jeu en réseau XMaze

### Projet de tutorat systeme s8

*BOUCKAERT Benoît*
*VICIOT Léa*

## Sommaire

- [Fonctionnement global](https://gitlab.com/BenBouck/tutsys_s8_ReX#fonctionnement-global)
- [Moteur de jeu](https://gitlab.com/BenBouck/tutsys_s8_ReX#moteur-de-jeu)

## Fonctionnement global

![Diagramme de flux](https://gitlab.com/BenBouck/tutsys_s8_ReX/-/blob/master/Diagramme_flux.jpeg "Diagramme de flux")

### Liste des sémaphores

- Liste des clients (liste chaînée)
- Envoi TCP d'un message dans le chat

## Moteur de jeu

Le moteur de jeu se charge de calculer les interractions entre les différents éléments du jeu : entités 
(joueurs et balles) et murs. Ces calculs se font sur une carte en 2 dimentions, vue de dessus.

### Entités et murs

Les entités et murs sont définies dans le fichier 'entites.h' tel que suit. Les entités représentent les
joueurs et les balles (tirs). Il s'agit de cercles, définis par un centre, un rayon et une orientation.
Les murs sont quant à eux définis par deux points.

### Collisions

Afin de prévenir des joueurs traversant les murs et de comptabiliser les coups dus aux tirs, les 
collisions sont gérées selon 2 cas de figure : 
- collision entre entités,
- collision entre entité et mur.

#### Collision entre entités

Les entités étant des cercles, leur collision est simple à repérer : si la distance entre leurs centres
est inférieure ou égale à la somme de leurs rayons, les entités se touchent ou sont imbriquées. Il y a donc
collision.

#### Collision avec un mur

Pour la collision entre une entité et un mur, la hitbox de l'entité est considérée comme un carré. On vérifie alors si le mur suit l'axe x ou y ; puis on vérifie si le carré formé par le centre de l'entité et son rayon sur les axes x ou y touche le mur.

### Déplacements

Le déplacement est géré sur l'axe x et y de la même manière :
- Stockage dans une variable tampon de la position
- Déplacement de vitesse*(cos|sin)(orientation) 
- Vérification des collisions (récursif sur les listes d'entités et de murs)
- Le cas échéant, restitution de l'ancienne valeur de déplacement
- Retour du type de collision